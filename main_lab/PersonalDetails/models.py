from django.db import models

# Create your models here.
class Doctors(models.Model):
	GENDER=(
		('M','Male'),
		('F','Female'),
	)
	first_name=models.CharField(max_length=30)
	last_name=models.CharField(max_length=20)
	gender=models.CharField(max_length=1,choices=GENDER)
	mobile_number=models.BigIntegerField(max_length=10)
	alternate_mobile_number=models.BigIntegerField(max_length=10)
	Email_id=models.EmailField()
	academics=models.CharField(max_length=200,blank=True)
	address=models.CharField(max_length=200)
	state=models.CharField(max_length=30)
	city=models.CharField(max_length=30)
	pin_code=models.IntegerField(max_length=6)
	experience=models.CharField(max_length=100)
	associated_with=models.CharField(max_length=50)
	def __str__(self):
		return self.first_name + self.last_name
	
		
#
