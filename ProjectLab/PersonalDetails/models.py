from django.db import models
from django_countries.fields import CountryField
# Create your models here.

class Patient(models.Model):
	GENDER=(
		('M','Male'),
		('F','Female')
		)
	First_name=models.CharField(max_length=30, blank=False)
	Last_name=models.CharField(max_length=20, blank=False)
	Gender=models.CharField(max_length=1, choices=GENDER, blank=False)
	DOB=models.DateField()
	Mobile_No=models.BigIntegerField(max_length=11, blank=False)
	Alternate_Mobile_No=models.BigIntegerField(max_length=11, blank=False)
	Email_Address=models.EmailField()
	Correspondence_Address=models.CharField(max_length=100, blank=False)
	country = CountryField()
	State=models.CharField(max_length=30, blank=False)
	City=models.CharField(max_length=30, blank=False)
	PinCode=models.IntegerField(max_length=6, blank=False)
	def __str__(self):
		return self.First_name + " " +self.Last_name
		
	
